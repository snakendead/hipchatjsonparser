//
//  HipChatParserXCTest.m
//  JSONStringParse
//
//  Created by Snake on 06.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "HipChatParser.h"
#import "Reachability.h"

@interface HipChatParserXCTest : XCTestCase

@property (strong, nonatomic) HipChatParser* parser;

@end

@implementation HipChatParserXCTest

- (HipChatParser*) parser {
    if (!_parser) _parser = [[HipChatParser alloc] init];
    return _parser;
}

//Test a simple string
- (void)testParseForJSONTypicalInput {
    
    NSString* neededString = @"@Chris, hello! It's a @Mike (cheers)(android) Check my blog! http://blog.net";
    
    NSURL *url = [NSURL URLWithString:@"http://blog.net"];
    NSDictionary* linkDict = [self.parser getDictAndTitleForUrlString:url];
    
    NSDictionary* rightOutput = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @[@"Chris", @"Mike"],@"mentions",
                                 @[@"cheers", @"android"],@"emoticons",
                                 @[linkDict],@"links", nil];
    
    __block NSDictionary* outputJSON = nil;
    
    [self.parser parseForJSON:neededString withCompletitionBlock:^(NSDictionary *result, NSError *error) {
        outputJSON = result;
        
        if (error) {
            NSLog(@"ERROR - %@", error);
            XCTFail();
        }
        
        XCTAssertEqualObjects(outputJSON, rightOutput);

    }];
}

// Test a string with many arguments, and string, that contains string like mention in url
- (void)testParseForJSONStringWithoutWhiteSpaces {
    
    NSString* neededString = @"@Chris@Mary@Mike(hello) http://weather.org/weather@today?big_data How are you? (cheers)(cheers) se ya at google.com";
    
    NSURL *url = [NSURL URLWithString:@"http://weather.org/weather@today?big_data"];
    NSURL *url2 = [NSURL URLWithString:@"http://google.com"];
    
    NSDictionary* linkDict = [self.parser getDictAndTitleForUrlString:url];
    NSDictionary* linkDict2 = [self.parser getDictAndTitleForUrlString:url2];
    
    NSDictionary* rightOutput = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @[@"Chris",@"Mary", @"Mike"],@"mentions",
                                 @[@"hello", @"cheers", @"cheers"],@"emoticons",
                                 @[linkDict, linkDict2],@"links", nil];
    
    __block NSDictionary* outputJSON = nil;
    
    [self.parser parseForJSON:neededString withCompletitionBlock:^(NSDictionary *result, NSError *error) {
        outputJSON = result;
        
        if (error) {
            NSLog(@"ERROR - %@", error);
            XCTFail();
        }
        
        XCTAssertEqualObjects(outputJSON, rightOutput);
    }];
    
}

- (void)testParseForJSONWithNonStringObject {
    
    id neededString = [NSArray arrayWithObjects:@"Hello", @"World", nil];
    
    [self.parser parseForJSON:neededString withCompletitionBlock:^(NSDictionary *result, NSError *error) {

        if (error) {
            NSLog(@"ERROR - %@", error);
            XCTAssertNil(result);
        } else {
            XCTFail();
        }
    }];
}

- (void)testParseForJSONWithNonASCCI {
    
    NSString* string = @"@Ÿoseph and WHAT?? @ξζρΞ at https://mail.google.com/mail/u/0/#inbox/14e6eac1cfb16f27";
    
    NSDictionary* rightOutput = [NSDictionary dictionaryWithObjectsAndKeys:
                                 @[@"Ÿoseph", @"ξζρΞ"],@"mentions",
                                 @[@"https://mail.google.com/mail/u/0/#inbox/14e6eac1cfb16f27"], @"links", nil];
    
    [self.parser parseForJSON:string withCompletitionBlock:^(NSDictionary *result, NSError *error) {
        
        if (error) {
            NSLog(@"ERROR - %@", error);
            XCTFail();
        } else {
            XCTAssertEqualObjects(result, rightOutput);
        }
    }];
}

@end
