//
//  ViewController.m
//  JSONStringParse
//
//  Created by Snake on 05.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import "ViewController.h"
#import "HipChatParser.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (strong, nonatomic) HipChatParser* parser;
@end

@implementation ViewController

- (HipChatParser*) parser {
    if (!_parser) _parser = [[HipChatParser alloc] init];
    return _parser;
}

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (IBAction)buttonAction:(UIButton *)sender {
    [self.parser parseForJSON:_textField.text withCompletitionBlock:^(NSDictionary *result, NSError *error) {
        NSLog(@"result %@", result);
    }];
}


@end
