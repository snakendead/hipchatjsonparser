//
//  HipChatParser.h
//  JSONStringParse
//
//  Created by Snake on 06.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HipChatParser : NSObject

- (void)    parseForJSON:(NSString*)string
   withCompletitionBlock:(void (^)(NSDictionary *result, NSError *error))completition;
- (NSDictionary*) getDictAndTitleForUrlString:(NSURL*)url;

@end
