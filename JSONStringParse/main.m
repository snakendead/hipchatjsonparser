//
//  main.m
//  JSONStringParse
//
//  Created by Snake on 05.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
