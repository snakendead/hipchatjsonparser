//
//  AppDelegate.h
//  JSONStringParse
//
//  Created by Snake on 05.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

