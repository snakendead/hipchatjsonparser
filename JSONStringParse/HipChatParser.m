//
//  HipChatParser.m
//  JSONStringParse
//
//  Created by Snake on 06.07.15.
//  Copyright (c) 2015 Snakendead. All rights reserved.
//

#import "HipChatParser.h"
#import <HTMLReader/HTMLReader.h>
#import "Reachability.h"

@interface HipChatParser ()

@property (copy, nonatomic) NSString* parseString;

@end

@implementation HipChatParser

#pragma mark - Public

// Method, that check input string, and return errors in cases of wrong input values
// First part check String by needed parts, only in this order
// Because we need to delete some parts of string in some cases (later)
// Making this async - because we use NSUrl Connection that can "freeze" app
- (void)    parseForJSON:(NSString*)string
   withCompletitionBlock:(void (^)(NSDictionary *result, NSError *error))completition {
    
    NSError* error = nil;
    
    if (string == nil) {
        error = [NSError errorWithDomain:@"Input string instance is nil"
                                    code:0 userInfo:nil];
    }
    
    if (![string isKindOfClass:[NSString class]]) {
        error = [NSError errorWithDomain:@"Input string instance is not kind of class NSString" code:1 userInfo:nil];
    }
    
    self.parseString = string;
    
    __block NSDictionary* result = nil;
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    if (!error) {
        dispatch_async(queue, ^{
            NSArray* URLArray =         [self parseForURL:self.parseString];
            NSArray* mentionsArray =    [self parseForMentionsInString:self.parseString];
            NSArray* emoticonArray =    [self parseForEmoticonsInString:self.parseString];
            
            result = @{     @"mentions":mentionsArray,
                            @"emoticons":emoticonArray,
                            @"links":URLArray};
            
            completition (result, error);
        });
    } else {
        completition (result, error);
    }

}

// Method, that return dictionary with link and title, for JSON format parsing of URL array

- (NSDictionary*) getDictAndTitleForUrlString:(NSURL*)url {
    NSString *siteTitle = nil;
    Reachability* reachability = [Reachability reachabilityForInternetConnection];
    if ([reachability currentReachabilityStatus] != NotReachable) {
        
        NSURLResponse* urlResponse = nil;
        NSError* error = nil;
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
        [request setTimeoutInterval:3.0];
        
        NSData *htmlData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&error];
        
        if (htmlData) {
            NSString *markUp = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
            if (markUp) {
                HTMLDocument *site = [HTMLDocument documentWithString:markUp];
                siteTitle = [site firstNodeMatchingSelector:@"title"].textContent;
            }
        }
    }
    if (siteTitle == nil) {
        siteTitle = url.absoluteString;
    }
    return @{@"url": url.absoluteString,
             @"title":siteTitle};
}

#pragma mark - Private

// Method, that parse a string on URL with Apple class NSDataDetector.
// If we find a URL - we must delete this chars in string, because it can be is not compatible
// with mentions. But if we find a URL like http://example.com/ex@late we know, that @late
// is not (!!!) a mention.
// Second - we download a HTML data (with framework) and find a page title for dictionary
// Methods try to get a data 3 sec, and if we don't get it - we duplicate url as title

#warning IMPORTANT! 
// I don't know what can be right - send url google.com like http://google.com
// or send as fact in string (google.com) - it's must be described in API.
// Now implemented with link format,
// it can be quick corrected - by (NSString *)substringWithRange:(NSRange)range;

- (NSArray*) parseForURL:(NSString*)string {
    
    NSMutableArray* URLArray = [[NSMutableArray alloc] init];
    NSError* error = nil;
    NSDataDetector* detector = [NSDataDetector
                                dataDetectorWithTypes:NSTextCheckingTypeLink error:&error];
    
    NSArray *matches = [detector matchesInString:string
                                         options:0
                                           range:NSMakeRange(0, [string length])];
    for (NSTextCheckingResult *match in matches) {
        if ([match resultType] == NSTextCheckingTypeLink) {
            NSRange matchRange = [match range];
            self.parseString = [self.parseString stringByReplacingCharactersInRange:matchRange withString:[[NSString string] stringByPaddingToLength:matchRange.length withString:@" " startingAtIndex:0]];
            NSURL *url = [match URL];
            
            NSDictionary* linkDict = [self getDictAndTitleForUrlString:url];
            
            [URLArray addObject:linkDict];
        }
    }
    
    return [URLArray copy];
}

// This method find a "@" symbol and separate full string by this.
// After, it separates all parts (except first, because it's havn't a "@") by non-word chars
// and we take first part of every separated string, what contains only alphanumerical chars

- (NSArray*) parseForMentionsInString:(NSString*)string {
    NSMutableArray* mentionsArray = [[NSMutableArray alloc] init];
    
    NSArray* stringComponents = [string componentsSeparatedByString:@"@"];
    if (stringComponents.count > 1) {
        for (int i = 1; i < stringComponents.count; i++) {
            NSString* partString = stringComponents[i];
            NSArray* partComponents = [partString componentsSeparatedByCharactersInSet:
                                       [[NSCharacterSet alphanumericCharacterSet] invertedSet]];
            if (![partComponents.firstObject isEqualToString:@""]) {
                [mentionsArray addObject:partComponents.firstObject];
            }
        }
    }
    return [mentionsArray copy];
}

// This method find symbols "(" and ")". Only if string contains both - we separate string by "("
// After this - we separate part by ")", and take first part of this.
// All separated parts we check for regular expression format, that contains 15 alphanumerical chars.
// If it validate - we find a emoticon
- (NSArray*) parseForEmoticonsInString:(NSString*)string {

    NSMutableArray* emoticonArray = [[NSMutableArray alloc] init];
    
    if ([string containsString:@"("] && [string containsString:@")"]) {
        NSArray* stringComponents = [string componentsSeparatedByString:@"("];
        if (stringComponents.count > 1) {
            for (int i = 1; i < stringComponents.count; i++) {
                NSString* partString = stringComponents[i];
                NSArray* finalComponent = [partString componentsSeparatedByString:@")"];
                NSString* expectedMention = finalComponent.firstObject;
                if (![expectedMention isEqualToString:@""]) {
                    if ([self validateEmoticon:expectedMention]) {
                        [emoticonArray addObject:expectedMention];
                    }
                }
            }
        }
    }
    return [emoticonArray copy];
}

- (BOOL)validateEmoticon:(NSString*)string {
    
    NSString *compare =     @"([a-zA-Z0-9]{1,15})";
    
    NSPredicate *urlTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", compare];
    return [urlTest evaluateWithObject:string];
}



@end
